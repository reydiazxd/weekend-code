import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const Form = () => {
  return (
    <form name="contact" method="post" netlify>
      <input type="hidden" name="form-name" value="contact" />
      <div className="form-group">
        <label htmlFor="input-email">Email address</label>
        <input name="email" type="email" className="form-control" id="input-email" aria-describedby="emailHelp" placeholder="Enter email..." />
      </div>
      <div className="form-group">
        <label htmlFor="input-name">Name</label>
        <input name="name" type="text" className="form-control" id="input-name" placeholder="Your name..." />
      </div>
      <div className="form-group">
        <label htmlFor="input-message">Message</label>
        <textarea name="message" className="form-control" id="input-message" placeholder="Your message..." />
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  )
}

class App extends Component {
  render () {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Weekend Code Project
          </p>
          <Form />
        </header>
      </div>
    );
  }
}

export default App;
