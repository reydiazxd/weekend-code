import fetch from 'node-fetch'
import { get, upperCase } from 'lodash'

const LBTC_API = 'https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/'

const getCurrencyRate = (base, divider) => (base > 0 && divider > 0) ? (base / divider) : 0

exports.handler = async (event, context) => {
  return fetch(LBTC_API)
    .then(resp => resp.json())
    .then(data => {
      const { base = 'VES', divider = 'CLP' } = event.queryStringParameters
      const baseCurrency = get(data, upperCase(base))
      const dividerCurrency = get(data, upperCase(divider))
      const basePrice = get(baseCurrency, 'rates.last', get(baseCurrency, 'avg_1h', 0))
      const dividerPrice = get(dividerCurrency, 'rates.last', get(dividerCurrency, 'avg_1h', 0))
      return {
        base: basePrice,
        divider: dividerPrice,
        relation: getCurrencyRate(basePrice, dividerPrice)
      }
    })
    .then(res => ({
      statusCode: 200,
      body: JSON.stringify(res)
    }))
    .catch(error => ({
      statusCode: 500,
      body: error.message || 'UNKNOWN_ERROR'
    }))
}
